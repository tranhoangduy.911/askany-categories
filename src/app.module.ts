import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CategoriesModule } from './categories/categories.module';
import { MongooseModule } from '@nestjs/mongoose/dist';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    CategoriesModule,
    ConfigModule.forRoot(),
   MongooseModule.forRoot('mongodb://localhost/experts')
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
