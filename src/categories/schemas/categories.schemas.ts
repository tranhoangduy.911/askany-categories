/* eslint-disable prettier/prettier */
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
export type CategoriesDocument = LogOrder & mongoose.Document;
// import { Owner } from '../owners/schemas/owner.schema';

@Schema({ collection: 'categories' })
export class LogOrder {
    @Prop({
        type: mongoose.SchemaTypes.ObjectId,
        default: () => new mongoose.Types.ObjectId(),
    })
    _id: string;

    @Prop({ type: String, required: false })
    name: string;

    @Prop({ type: String, required: false })
    ask: string;

    @Prop({ type: String, required: false })
    description: string;

    @Prop({ type: String, required: false })
    slug: string;

    @Prop({ type: String, required: false, default: 'vi', enum: ['en', 'vi'] })
    lang: { type: String, default: 'vi', enum: ['vi', 'en'] }

    @Prop({ type: Boolean, default: true })
    displayMenuBlog: boolean;

    @Prop({ type: Number, default: 1 })
    point: number;

    @Prop({ type: Number, default: 1, enum: [1, 0, -1], required: false })
    status: number

    /* SEO meta */
    @Prop({ type: String, required: false })
    seo_title: string;

    @Prop({ type: String, required: false })
    seo_description: string;

    @Prop({ type: String, required: false })
    seo_schema: string;

    @Prop({ type: String, required: false })
    seo_keywords: string;

    @Prop({ type: String, required: false })
    seo_canonical: string;

    @Prop({ type: String, required: false })
    seo_redirect: string;

    @Prop({ type: String, required: false })
    seo_lang: string;

    @Prop([String])
    recommendTags: string[];

    // @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'position' }] })
    // listPositions: Position[],

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'categories' })
    parent: mongoose.Types.ObjectId;

    // default
    @Prop({ type: Date, default: mongoose.now(), required: false })
    createdAt: Date;

    @Prop({ type: Date, default: mongoose.now(), required: false })
    modifiedAt: Date;
}

const CategorieSchema = SchemaFactory.createForClass(LogOrder);
// CategorieSchema.index({
//   province: 'text',
// });
export { CategorieSchema };
