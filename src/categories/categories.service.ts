import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { CategorieSchema, CategoriesDocument } from './schemas/categories.schemas';
import { HttpService } from '@nestjs/axios';
@Injectable()
export class CategoriesService {
  constructor(
    @InjectModel('categories')
    protected readonly MODEL: Model<CategoriesDocument>,
    private readonly httpService: HttpService,
  ) {
    // super(MODEL);
  }
  async handleGetAll(obj) {
    const signalGet = await this.MODEL.find({}).populate({path: "parent"});
    return { message: 'PHƯƠNG YÊU THƯƠNG', data: signalGet };
  }
}
