import { Module } from '@nestjs/common';
import { CategoriesService } from './categories.service';
import { CategoriesController } from './categories.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { HttpModule } from '@nestjs/axios';
import { CategorieSchema } from './schemas/categories.schemas';
@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'categories', schema: CategorieSchema }]),
    HttpModule,
  ],
  providers: [CategoriesService],
  controllers: [CategoriesController]  
})
export class CategoriesModule {}
