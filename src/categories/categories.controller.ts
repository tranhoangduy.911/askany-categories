import {
    Body,
    ConsoleLogger,
    Controller,
    Get,
    Post,
    Req,
    Res,
} from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { CategoriesService } from './categories.service';
@Controller('categories')
export class CategoriesController {
    constructor(private readonly CategoriesService: CategoriesService) { }
    @MessagePattern('getAllCategories')
    handleGetList(data) {
        const signalGetData = this.CategoriesService.handleGetAll(data);
        return signalGetData;
    }
}
